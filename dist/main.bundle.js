webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<app-lander-v2></app-lander-v2>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__lander_v2_lander_v2_component__ = __webpack_require__("../../../../../src/app/lander-v2/lander-v2.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lander_v2_risk_selector_risk_selector_component__ = __webpack_require__("../../../../../src/app/lander-v2/risk-selector/risk-selector.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { NewFilterService } from './dashboard/packages/package-search/new-filter.service'
// import { WindowRefService } from './data/window-ref.service'
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__lander_v2_lander_v2_component__["a" /* LanderV2Component */],
            __WEBPACK_IMPORTED_MODULE_6__lander_v2_risk_selector_risk_selector_component__["a" /* RiskSelectorComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/lander-v2/lander-v2.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-Light.ttf") + ") format(\"truetype\");\r\n  font-weight: 300;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-LightItalic.ttf") + ") format(\"truetype\");\r\n  font-weight: 300;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-SemiBold.ttf") + ") format(\"truetype\");\r\n  font-weight: 500;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-SemiboldItalic.ttf") + ") format(\"truetype\");\r\n  font-weight: 500;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-Bold.ttf") + ") format(\"truetype\");\r\n  font-weight: 600; }\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-BoldItalic.ttf") + ") format(\"truetype\");\r\n  font-weight: 600;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-ExtraBold.ttf") + ") format(\"truetype\");\r\n  font-weight: 700;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-UltraBold.ttf") + ") format(\"truetype\");\r\n  font-weight: 800;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-Black.ttf") + ") format(\"truetype\");\r\n  font-weight: 900;\r\n  font-style: italic;\r\n\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-UltraLight.ttf") + ") format(\"truetype\");\r\n  font-weight: 100;\r\n\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-UltraLightItalic.ttf") + ") format(\"truetype\");\r\n  font-weight: 100;\r\n  font-style: italic;\r\n\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-Regular.ttf") + ") format(\"truetype\");\r\n  font-weight: 400;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-Italic.ttf") + ") format(\"truetype\");\r\n  font-weight: 400;\r\n  font-style: italic;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-ExtraLight.ttf") + ") format(\"truetype\");\r\n  font-weight: 200;\r\n}\r\n@font-face {\r\n  font-family: \"Thai Sans Neue\";\r\n  src: url(" + __webpack_require__("../../../../../src/assets/media/font/ThaiSansNeue-ExtraLightItalic.ttf") + ") format(\"truetype\");\r\n  font-weight: 200;\r\n  font-style: italic;\r\n}\r\n\r\n\r\n/*Landing*/\r\nbody,\r\np,\r\na,\r\nbutton,\r\nspan,\r\ninput,\r\nol,\r\nul {\r\n  font-family: 'Sukhumvit Set', 'Thai Sans Neue', sans-serif;\r\n\r\n}\r\n\r\nh1,\r\nh2,\r\nh3,\r\nh4,\r\nh5,\r\nh6,\r\nu {\r\n  font-family: 'Sukhumvit Set', 'Thai Sans Neue', sans-serif;\r\n  font-weight: 600;\r\n}\r\n\r\nh1 {\r\n  font-size: 50px;\r\n}\r\n\r\nh2 {\r\n  font-size: 45px;\r\n  font-weight: 300;\r\n\r\n}\r\n\r\nh3 {\r\n  font-size: 25px;\r\n  font-weight: 400;\r\n\r\n}\r\n\r\nh4 {\r\n  font-size: 20px;\r\n\r\n}\r\n\r\nh5 {\r\n  font-size: 20px;\r\n  font-weight: 400;\r\n}\r\n\r\nh6 {\r\n  font-size: 19px;\r\n  font-weight: 500;\r\n}\r\n\r\np {\r\n  font-size: 20px;\r\n  font-weight: 300;\r\n}\r\n\r\nbutton {\r\n  cursor: pointer;\r\n}\r\n.pt-40 {\r\n  padding-top: 40px !important;\r\n}\r\n\r\n/* Start Nav */\r\n.navbar {\r\n  background: none;\r\n  box-shadow: none;\r\n  opacity: 1;\r\n  z-index: 9999;\r\n  padding: 0;\r\n  padding-top: 30px;\r\n  position: absolute;\r\n  width: 100%;\r\n}\r\n\r\n.navbar-brand {\r\n  margin-right: 0;\r\n}\r\n.navbar-brand img {\r\n  margin-top: -10px;\r\n}\r\n\r\n.navbar-toggler {\r\n  border: 0;\r\n  border-radius: 0;\r\n}\r\n.navbar-toggler span {\r\n  background-color: #fff;\r\n  display: block;\r\n  height: 4px;\r\n  margin-bottom: 5px;\r\n  width: 36px;\r\n}\r\n.navbar-toggler span:last-child {\r\n  margin-bottom: 0;\r\n}\r\n\r\n.nav-item a {\r\n  font-family: 'Sukhumvit Set';\r\n}\r\n\r\n.navbar-light .navbar-nav .nav-link {\r\n  color: white;\r\n  font-family: 'Sukhumvit Set', 'Thai Sans Neue', sans-serif;\r\n  font-weight: 400;\r\n  font-size: 18px;\r\n}\r\n\r\n.facebook {\r\n  background-image: url(" + __webpack_require__("../../../../../src/assets/lander/facebook_nocircle.png") + ");\r\n  background-position: center center;\r\n  background-repeat: no-repeat;\r\n  display: block;\r\n  height: 47px;\r\n  width: 47px;\r\n}\r\n\r\n.nav-link {\r\n  cursor: pointer;\r\n}\r\n\r\n.call-me {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/img/phone.png") + ");\r\n  background-repeat: no-repeat;\r\n  background-position: 15px center;\r\n  background-size: 20px;\r\n  border: 3px solid white;\r\n  border-radius: 50px;\r\n  color: white;\r\n  display: block;\r\n  padding: 7px 20px;\r\n  padding-left: 40px;\r\n  text-decoration: none;\r\n }\r\n\r\n.call-me a {\r\n  color: white;\r\n  text-decoration: none;\r\n}\r\n\r\n.facebook-icon {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/lander/facebook_nocircle.png") + ");\r\n  padding: 4px 20px;\r\n  padding-left: 40px;\r\n  background-repeat: no-repeat;\r\n  background-position: 15px center;\r\n  background-size: 35px;\r\n}\r\n\r\n.btn-blue {\r\n  background: #33C4EF;\r\n  box-shadow: 0 3px 9px 0 rgba(51, 196, 239, 0.7);\r\n  border-radius: 100px;\r\n  border: none;\r\n  color: white;\r\n  font-weight: 500;\r\n  font-size: 20px;\r\n  padding: 8px 40px;\r\n }\r\n\r\n@media (max-width: 991px) {\r\n  .navbar-brand {\r\n    float: right;\r\n  }\r\n  .navbar-brand img {\r\n    margin-top: 0;\r\n  }\r\n  .navbar {\r\n    height: 68px;\r\n    padding-top: 20px;\r\n  }\r\n\r\n  .navbar-brand img {\r\n    width: 120px;\r\n  }\r\n  .navbar-nav {\r\n    margin-top: 30px;\r\n    background: #36b5e2;\r\n    display: block;\r\n    padding: 0;\r\n    list-style: none;\r\n    }\r\n\r\n    .navbar-nav li {\r\n      padding: 10px;\r\n      padding-left: 30px;\r\n    }\r\n    .navbar-nav li:hover {\r\n      background-color: #4DCCF4;\r\n    }\r\n\r\n    .call-me {\r\n      display: inline-block;\r\n    }\r\n}\r\n@media (max-width: 767px) {\r\n  .navbar-brand {\r\n    margin-right: 20px;\r\n  }\r\n  .navbar-brand img {\r\n    width: 100px;\r\n    margin-top: -15px;\r\n  }\r\n  .navbar .container {\r\n    width: 100%;\r\n  }\r\n  .navbar-toggler span {\r\n    height: 3px;\r\n    width: 30px;\r\n  }\r\n}\r\n  /* End Nav */\r\n\r\n/* Start Header */\r\n.oc-hero-section {\r\n  background: #56D5F6;\r\n  background: linear-gradient(to bottom right, #56D5F6, #0786C6);\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom;\r\n  background-size: cover;\r\n  position: relative;\r\n  height: 120vh;\r\n  min-height: 750px;\r\n }\r\n .oc-hero-section .head {\r\n   padding: 122px 0 30px 0;\r\n }\r\n.oc-hero-section h1 {\r\n  margin-bottom: 16px;\r\n}\r\n.filter-container {\r\n  background: #FFFFFF;\r\n  box-shadow: 0 3px 8px 0 rgba(17, 58, 70, 0.15);\r\n  border-radius: 4px;\r\n  padding: 25px;\r\n  margin: 0 40px;\r\n}\r\n.graph-bg {\r\n  /* For Safari 5.1 to 6.0 */\r\n  /* For Opera 11.1 to 12.0 */\r\n  /* For Firefox 3.6 to 15 */\r\n  background: linear-gradient(rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0.5) 100%), url(" + __webpack_require__("../../../../../src/assets/img/graph-bg.png") + ");\r\n  /* Standard syntax */\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom;\r\n  background-size: 100vw;\r\n  height: 120vh;\r\n  min-height: 750px;\r\n}\r\n\r\n.cta-btn {\r\n  max-width: 400px;\r\n  height: 65px;\r\n  margin: 30px;\r\n  background: white;\r\n  color: #666666;\r\n}\r\n\r\n.pt-80 {\r\n  padding-top: 80px !important;\r\n}\r\n\r\n.pb-80 {\r\n  padding-bottom: 80px !important;\r\n}\r\n\r\n.ptb-40 {\r\n  padding-top: 40px;\r\n  padding-bottom: 40px;\r\n}\r\n\r\n.testuse__title {\r\n  color: white;\r\n}\r\n\r\n.testuse__detail {\r\n  font-size: 30px;\r\n  font-weight: 400;\r\n  line-height: 1.26em;\r\n  color: white;\r\n }\r\n\r\nlabel.chevron {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 100%;\r\n}\r\nlabel.chevron:after {\r\n  content: '\\25BE';\r\n  position: absolute;\r\n  width: 24px;\r\n  color: #2287A5;\r\n  font-weight: bold;\r\n  font-size: 18px;\r\n  right: 2px;\r\n  bottom: 6px;\r\n  pointer-events: none;\r\n  z-index: 2;\r\n  font-family: initial;\r\n}\r\nlabel.chevron select {\r\n  position: relative;\r\n  width: 100%;\r\n  -webkit-appearance: none;\r\n  -moz-appearance: none;\r\n  appearance: none;\r\n  background: #fff;\r\n  color: #7D7C7D;\r\n  border: 1px solid #CFCFCF;\r\n  outline: none;\r\n  font-size: 16px;\r\n  padding: 8px 9px 6px 40px;\r\n  margin: 0;\r\n  border-radius: 3px;\r\n  cursor: pointer;\r\n  height: 38px;\r\n  line-height: 1.25em;\r\n}\r\nlabel.chevron select.form-location {\r\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAQCAYAAADNo/U5AAAAAXNSR0IArs4c6QAAAbRJREFUKBVtUjsvRGEQPXP37hLiEZEIhcQjIlGS2ML6AduKDUFNLViPAg2WX6AWISuUEgqFVSgoFBJWkGwjEWGDiMfd+5n5+PZej2nmzJlzZr7JvQRfNMyvtnI5CqIIFCpBuINSKeaWLif6jo2UBLQuHwWz9+lphnEFZZumyQRyGCfKK5pmjwfbPrQg+3Axw+JJI/qdvwdNsU6WTFFjYj3sKveAnxEw4pKCIOorSnF1/4intw9Dg5+ds8jqsNkw4jdEm2uRiIZRFLLx8u4gvn2I7bPMl5EHu3BHLD64w4wqsANYiLZrg3BilFr4fLDeAlSZIWpKi1AcCppSZ6mF90KVsYm+dwOZh2fcPL54fUZSC+8FZfgu7BsipxSGtvZxfpvlM5XOUgtvQvTUtLgWcXJu3miatkVwXE+c5wNWp5Ue600R0Z4hTf7PIDrR801AANYwEXwfxFi9LH3RCaNN6fGeE/7UM57kL+It06KTjv73BPDh1JBY22DQJfWPINq8jPd2s1EfqTeJQIiqkD3AcNdvYH6ntqq63xi01i8Q3JJMhl6vnTneOMCTVgrr7MnTWOzdr/sEsfigVbAUu70AAAAASUVORK5CYII=);\r\n  background-repeat: no-repeat;\r\n  background-position: 15px center;\r\n}\r\nlabel.chevron select.form-gender {\r\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAARCAYAAADZsVyDAAAAAXNSR0IArs4c6QAAAvVJREFUOBGlVE1IVFEUPue+eePfQjEjcxGoUwYWUbZroRIu+gGFcHAURQkqDCIUHIMWRlBOSj8ilbgIEid7URH2o5AptDFIq0VQ/kEUURRaSuM4b947natdeZpZ0Fu88/fd75x77rkX4T++nLbneij0LsXU4ibHju2dc1Kh0/hXPfu8kRyORK8QUQETTBBiOgI9Q4w9NOY/8EHy/Eac0WgkCrRywbZjyC0Gx2t9750JPS3GWvoRfYICz21P14K3vF6LE+Cmppv7LNtqRre+f7zGOyacizYGggVI5rBNVGyjyKcIPfY0BmucGDtknkCEa2N+X4cklTFEpNG6kvtcZQNEoqelb5E4s6lri01wCUVs7kR9aflEva96Q+r6bZxkjyfQWSbB8kOCQg0T2hespf9yf6nB1ec2EIlFYrKs45zxjOqRXDJQlR92u7WjnNCvKAgw4a2/cEbZTtmAaHNzv3Y0301ZJEbAPK7knhMo9Te1JSMcc21uvbNmeWxFm5Di9DDOE2cbhhsI3H+qBIBGrNk5jyTiXUU8LQ9jViSVcaRkSnNNzROHx82tBPRK9iYrcCNNLZJjldPWHc9sQ5Zl71jw0yOY/XZQYZwyMxAs4la9eO31Rn61QlRqAluuB7paTaKrChyOWNVTUzO9hNDF7SgsNgwNdf2SbUOFwiyRBBUuohbpc2Wc7fQT2lU24W7echwS9Sgw65+42nVgQQ/L2OEJ08B4/QiakKAwTskTkQCofZE+oWl6L4JWrOuiiPv3UdNEUIHJjb2sR2M1fRefQTmfYSuFrCIe0kGFWSJ5ImywUqVPjNZ5XwJZPjNiPeDB7xyp8z1VYHnrkLA9bJl9HPtMZBfzrirjYuCkwjglCtdl3vlF+YZwkQByKmTDnSCly8P7PjldR8AVC7iQlJTVNnR4p6niy2Vef79rID8/Ok+8PKhs+S7wFe5DEN0YnxjgF2xaxf4mXasB7NloGaC4Pe73nVoNt1JsVWIX4hAImj/llRav5vsJSbgxaz0HkfAAAAAASUVORK5CYII=);\r\n  background-repeat: no-repeat;\r\n  background-position: 10px center;\r\n }\r\nlabel.chevron select.form-age {\r\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAV1JREFUOBGtVL9Lw0AUfl9ykVZQFwXnFtwEUXD0DxCchIJ0EJykiAgWKYiTiHXp4OZuEcRBieK/oYOT1FUHIbEiigSf79IGYtIepHhweXfv+0HuyxFQYsyduMO+395mxjKI3VatvBdRTJiKSFH1vPYtMS0Q8XOu6OxHfV1NmBUndtejUn9g4fShVPpO4CYsQR1wi6SuWD87IOZNAo5btZXdOG7C0kdjrrLNs2JWjZuEawOWMmLioaed8qOuSSMThkL9fIwoaIBpSYgTSbFpD+BVvvDNSC6/JZ8/aMgx1tik6IMx87hAq+9fn2TpN+nDy9Je1Bnlsyh6cSUSJUZ83wvM0gPoziJlr4ci4E2qJ/fnAsqeVrBnJMzLsNfpu8qx5/WU3jWBfJkfWgvlbOhKhcPmIFn/0abuUYgO8PhnI8ln6qipz55phJpOttT9H6ESMF1JVpNZnALGi0Rd0ZpfzKONMk5MEQ0AAAAASUVORK5CYII=);\r\n  background-repeat: no-repeat;\r\n  background-position: 10px center;\r\n}\r\n @media (max-width: 991px) {\r\n  .filter-container {\r\n    margin: 0 20px;\r\n  }\r\n  .oc-hero-section,\r\n  .graph-bg {\r\n    height: 130vh;\r\n  }\r\n  .oc-hero-section .head {\r\n    padding: 20px 0 30px 0;\r\n  }\r\n\r\n}\r\n@media (max-width: 767px) {\r\n  .oc-hero-section,\r\n  .graph-bg {\r\n    height: 120vh;\r\n  }\r\n  .graph-bg{\r\n    background-size: 188%;\r\n  }\r\n  .oc-hero-section .head {\r\n    padding: 50px 0 30px 0;\r\n  }\r\n  .oc-hero-section h1 {\r\n    font-size: 28px;\r\n    margin-top: 10px;\r\n  }\r\n  .testuse__detail {\r\n    font-size: 24px;\r\n  }\r\n  .testuse__detail br {\r\n    display: none;\r\n  }\r\n  .filter-container {\r\n    margin: 0;\r\n  }\r\n  .pt-80 {\r\n    padding-top: 40px !important;\r\n  }\r\n\r\n  .pb-80 {\r\n    padding-bottom: 40px !important;\r\n  }\r\n}\r\n/* End Header */\r\n\r\n/* Start screw */\r\n.screw {\r\n  content: \"\";\r\n  position: relative;\r\n  width: 100%;\r\n  height: 127px;\r\n  left: 0;\r\n  top: -126px;\r\n  background-color: white;\r\n  -webkit-clip-path: polygon(100% 20%, -3% 100%, 100% 100%);\r\n  clip-path: polygon(100% 20%, -3% 100%, 100% 100%);\r\n}\r\n@media (max-width: 767px) {\r\n  .screw {\r\n    height: 137px;\r\n    top: -136px;\r\n    -webkit-clip-path: polygon(100% 20%, -3% 100%, 100% 100%);\r\n    clip-path: polygon(100% 50%, -21% 100%, 100% 100%);\r\n  }\r\n}\r\n/* End screw */\r\n\r\n/* Start section1 */\r\n.section1 {\r\n  z-index: 1;\r\n  margin-top: -240px;\r\n}\r\n\r\n.section1 h3 {\r\n  font-weight: 500;\r\n  font-size: 36px;\r\n  color: #4A4A4A;\r\n  margin: 38px 0 10px;\r\n}\r\n\r\n.section1 p {\r\n  font-weight: 400;\r\n  font-size: 30px;\r\n  color: #646465;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.section1 span {\r\n  font-weight: 600;\r\n  font-size: 20px;\r\n  color: #60B0D5;\r\n  line-height: 35px;\r\n  text-align: left;\r\n}\r\n\r\n.section1 .mini {\r\n  position: absolute;\r\n  top: -240px;\r\n}\r\n\r\n.section1 .gage {\r\n  position: relative;\r\n  left: -130px;\r\n  top: 150px;\r\n}\r\n@media (max-width: 991px){\r\n  .section1 {\r\n    margin-top: -250px;\r\n  }\r\n}\r\n@media (max-width: 767px){\r\n  .section1 {\r\n    margin-top: -290px;\r\n  }\r\n  .section1 p{\r\n    font-size: 25px;\r\n  }\r\n  .section1 .tex-center{\r\n      text-align: center;\r\n  }\r\n  .section1 .occlogo {\r\n    width: 60%\r\n  }\r\n}\r\n/* End section1 */\r\n\r\n/* Start section2 */\r\n.section2 .hra-title {\r\n  font-weight: bold;\r\n  color: #45C8F0;\r\n  padding-bottom: 20px;\r\n  font-size: 30px;\r\n}\r\n\r\n.section2 .feature-item {\r\n  padding: 6px 20px;\r\n  color: #646465;\r\n}\r\n\r\n.section2 .feature-item h5 {\r\n  font-size: 20px;\r\n  font-weight: 500;\r\n}\r\n\r\n.section2 a,\r\n.section2 span {\r\n  font-weight: normal;\r\n  font-size: 18px;\r\n  color: #60B0D5;\r\n  padding-left: 20px;\r\n}\r\n\r\n.section2 a {\r\n  padding-top: 20px;\r\n  text-decoration: underline;\r\n}\r\n\r\n.section2 span {\r\n  padding-bottom: 20px;\r\n  font-size: 14px;\r\n}\r\n\r\nh1 {\r\n  font-weight: 600;\r\n  font-size: 50px;\r\n  color: #316B95;\r\n  letter-spacing: 0;\r\n  line-height: 47px;\r\n  margin-bottom: 40px;\r\n}\r\n\r\n.section2 .package {\r\n  height: 350px;\r\n  border-radius: 4px;\r\n  box-shadow: 0 3px 8px rgba(17, 58, 70, 0.15);\r\n  position: relative;\r\n}\r\n\r\n.section2 .package .pkg-desc {\r\n  background: linear-gradient(rgba(129, 129, 129, 0) 0%, rgba(77, 77, 77, 0.5) 100%);\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom;\r\n  position: absolute;\r\n  bottom: 0;\r\n  width: 100%;\r\n  border-radius: 4px;\r\n}\r\n\r\n.section2 .package h5,\r\n.section2 .carousel-caption h3 {\r\n  font-weight: 600;\r\n  font-size: 24px;\r\n  color: #FFFFFF;\r\n  line-height: 35px;\r\n  padding: 20px 20px 0;\r\n}\r\n\r\n.section2 .package span,\r\n.section2 .carousel-caption p span {\r\n  font-weight: 300 !important;\r\n  font-size: 24px !important;\r\n  color: #FFFFFF !important;\r\n  letter-spacing: 0 !important;\r\n  line-height: 35px !important; }\r\n\r\n.section2 .package span.price,\r\n.section2 .carousel-caption p span.price {\r\n  font-weight: 600 !important;\r\n  font-size: 30px !important;\r\n}\r\n.section2 .carousel-indicators li {\r\n  background: #5bc0de;\r\n  top: 70px;\r\n  max-width: 15px;\r\n  width: 15px;\r\n  height: 15px;\r\n  border-radius: 50%;\r\n}\r\n.section2 .carousel-indicators li.active {\r\n  background: #0089FF;\r\n}\r\n.section2 .carousel-caption {\r\n  left: 0;\r\n  right: 0;\r\n}\r\n.section2 .carousel-control-next,\r\n.section2 .carousel-control-prev {\r\n  width: 75px;\r\n  height: 75px;\r\n  background: #fff;\r\n  opacity: 1;\r\n  border-radius: 50%;\r\n  top: 38%;\r\n  box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3)\r\n}\r\n.section2 .carousel-control-next{\r\n  right: -25px;\r\n}\r\n.section2 .carousel-control-prev {\r\n  left: -25px;\r\n}\r\n.section2 .carousel-control-prev i,\r\n.section2 .carousel-control-next i {\r\n  top: 14px;\r\n  position: absolute;\r\n  color: black;\r\n  font-size: 50px;\r\n}\r\n.section2 .carousel-control-prev i.fa-angle-left {\r\n  left: 30px;\r\n}\r\n.section2 .carousel-control-next i.fa-angle-right {\r\n  right: 24px;\r\n}\r\n.section2 .bgpkg1 {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/img/p1.png") + ");\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  background-position: center;\r\n}\r\n\r\n.section2 .bgpkg2 {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/img/p2.png") + ");\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  background-position: center;\r\n}\r\n\r\n.section2 .bgpkg3 {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/img/p3.png") + ");\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  background-position: center;\r\n}\r\n\r\n.section2 .slide-show-none {\r\n  display: none;\r\n}\r\n@media (max-width: 991px){\r\n  .section2 .slide-show {\r\n    display: none !important;\r\n  }\r\n  .section2 .slide-show-none {\r\n    display: block !important;\r\n  }\r\n}\r\n@media (max-width: 767px){\r\n  h1 {\r\n    font-size: 45px;\r\n    margin-bottom: 30px;\r\n    text-align: center;\r\n  }\r\n  .section2 {\r\n    padding-bottom: 40px;\r\n  }\r\n  .section2 .carousel-control-next,\r\n  .section2 .carousel-control-prev {\r\n    width: 50px;\r\n    height: 50px;\r\n    top: 40%;\r\n  }\r\n  .section2 .carousel-control-next{\r\n    right: -15px;\r\n  }\r\n  .section2 .carousel-control-prev {\r\n    left: -15px;\r\n  }\r\n  .section2 .carousel-control-prev i,\r\n  .section2 .carousel-control-next i {\r\n    top: 10px;\r\n    font-size: 35px;\r\n  }\r\n  .section2 .carousel-control-prev i.fa-angle-left {\r\n    left: 20px;\r\n  }\r\n  .section2 .carousel-control-next i.fa-angle-right {\r\n    right: 20px;\r\n  }\r\n}\r\n@media (max-width: 575px){\r\n  h1 {\r\n    font-size: 30px;\r\n    margin-bottom: 20px;\r\n\r\n  }\r\n}\r\n/* End section2 */\r\n\r\n/* Start section3 */\r\n.section3 {\r\n  background: #F3F3F4;\r\n}\r\n\r\n.section3 .pplcard {\r\n  background: #FFFFFF;\r\n  box-shadow: 0 0 24px 0 rgba(23, 76, 100, 0.24);\r\n  padding: 20px;\r\n}\r\n\r\n.section3 .pplcard h6 {\r\n  display: inline-block;\r\n  padding-left: 15px;\r\n  font-weight: 600;\r\n  font-size: 20px;\r\n  color: #4A4A4A;\r\n}\r\n\r\n.section3 .pplcard h6 small {\r\n  font-weight: 300;\r\n  font-size: 18px;\r\n  color: #9B9B9B;\r\n}\r\n\r\n.section3 .pplcard p {\r\n  margin-top: 15px;\r\n  font-weight: 400;\r\n  font-size: 18px;\r\n  color: #646465;\r\n}\r\n\r\n.section3 .pplcard .highlight {\r\n  color: #33C4EF;\r\n}\r\n/* End section3 */\r\n\r\n/* Start section4 */\r\n.section4 {\r\n  background: url(" + __webpack_require__("../../../../../src/assets/img/ocbg.png") + ");\r\n  background-repeat: no-repeat;\r\n  background-size: 80%;\r\n  background-position: center 40px;\r\n}\r\n\r\n.section4 h3 {\r\n  font-weight: 500;\r\n  font-size: 32px;\r\n  color: #4A4A4A;\r\n  margin: 38px 0 5px;\r\n}\r\n\r\n.section4 p {\r\n  font-weight: 400;\r\n  font-size: 28px;\r\n  color: #646465;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.section4 .hra-title {\r\n  font-size: 44px;\r\n  margin-bottom: 0;\r\n}\r\n\r\n.section4 .search {\r\n  top: -2px;\r\n  position: relative;\r\n  padding-right: 15px;\r\n}\r\n/* End section4 */\r\n\r\n/* Start footer */\r\nfooter {\r\n  background: #56D5F6;\r\n  background: linear-gradient(to bottom right, #56D5F6, #0786C6);\r\n  background-repeat: no-repeat;\r\n  background-position: center bottom;\r\n  background-size: cover;\r\n  position: relative;\r\n  min-height: 550px;\r\n  /* height: 50vh; */\r\n  color: white;\r\n}\r\n\r\nfooter .graph-bg {\r\n  background-image: url(" + __webpack_require__("../../../../../src/assets/img/graph-bg-mobile.png") + ");\r\n  background-repeat: no-repeat;\r\n  /* background-position: center calc(0% + 80px); */\r\n  background-size: 100vw;\r\n  min-height: 550px;\r\n  height: 80vh;\r\n}\r\n\r\nfooter .graph-bg:before {\r\n  background: linear-gradient(rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0.5) 100%);\r\n  bottom: 0;\r\n  content: '';\r\n  left: 0;\r\n  position: absolute;\r\n  right: 0;\r\n  top: 0;\r\n}\r\n\r\nfooter a {\r\n -webkit-text-decoration-color: white;\r\n         text-decoration-color: white;\r\n}\r\nfooter a p{\r\n  font-weight: 300;\r\n  font-size: 24px;\r\n  color: white;\r\n  margin-bottom: 0;\r\n}\r\n\r\nfooter h3 {\r\n  font-weight: 500;\r\n  font-size: 34px;\r\n  margin-bottom: 25px;\r\n}\r\n\r\nfooter h5 {\r\n  font-weight: 600;\r\n  font-size: 24px;\r\n  letter-spacing: 1.5px;\r\n}\r\n\r\nfooter p {\r\n  font-weight: 100;\r\n  font-size: 24px;\r\n  color: #FFFFFF;\r\n  text-align: center;\r\n}\r\n\r\n.contact{\r\n  text-align: center;\r\n}\r\n.contact p{\r\n  margin: 0;\r\n  font-size: 24px;\r\n  font-weight: 400;\r\n  letter-spacing: 1px;\r\n}\r\n.contact .call-me {\r\n  margin-top: 30px;\r\n  margin-bottom: 25px;\r\n  display: inline-block;\r\n  border:2px solid white;\r\n  padding: 4px 45px;\r\n  padding-right: 25px;\r\n  font-size: 20px;\r\n  font-weight: 500;\r\n  background-size: 15px;\r\n  background-position: 18px center;\r\n }\r\n\r\n.default-button {\r\n  background: #33C4EF;\r\n  box-shadow: 0 3px 9px 0 rgba(51, 196, 239, 0.7);\r\n  border-radius: 100px;\r\n  font-weight: 500;\r\n  font-size: 16px;\r\n  color: #FFFFFF;\r\n  padding: 8px 34px;\r\n  margin: auto;\r\n  border: none;\r\n}\r\n.default-button .search {\r\n  padding-right: 8px;\r\n}\r\n\r\n@media (max-width: 1199px) {\r\n  footer .graph-bg {\r\n    background-size: cover;\r\n  }\r\n  .default-button {\r\n    margin-top: 10px;\r\n    width: 100%;\r\n  }\r\n}\r\n@media (max-width: 991px) {\r\n  footer .logo img {\r\n    width: 180px;\r\n    margin-bottom: 20px;\r\n  }\r\n}\r\n@media (max-width: 767px) {\r\n  footer .logo img {\r\n    width: 130px;\r\n    margin-bottom: 10px;\r\n  }\r\n  footer h5 {\r\n    margin-top:30px;\r\n  }\r\n}\r\n/* End footer */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/lander-v2/lander-v2.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<!--Start Nav-->\r\n<nav class=\"navbar navbar-toggleable-md navbar-light bg-faded \">\r\n  <div class=\"container\">\r\n    <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n      <span></span>\r\n      <span></span>\r\n      <span></span>\r\n    </button>\r\n    <a class=\"navbar-brand\">\r\n        <img src=\"assets/lander/logo.png\" alt=\"Logo\">\r\n    </a>\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n    <ul class=\"navbar-nav ml-auto\">\r\n      <li class=\"nav-item ml-lg-5\">\r\n        <a class=\"nav-link\">สุขภาพดีกับโอแคร์</a>\r\n      </li>\r\n      <li class=\"nav-item ml-lg-5\">\r\n        <a class=\"nav-link\" (click)=\"signup()\">สมัครสมาชิก</a>\r\n      </li>\r\n      <li class=\"nav-item ml-lg-5\">\r\n        <a class=\"nav-link\" (click)=\"signin()\">เข้าสู่ระบบ</a>\r\n      </li>\r\n      <li class=\"nav-item ml-lg-4\">\r\n        <a class=\"facebook\" href=\"https://www.facebook.com/optimize.care/\"></a>\r\n      </li>\r\n      <li class=\"nav-item ml-lg-4\">\r\n        <a class=\"call-me\" href=\"tel:0991319919\">099-131-9919</a>\r\n      </li>\r\n    </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n<!--End Nav-->\r\n\r\n<!--<router-outlet></router-outlet>-->\r\n\r\n<!--Start Header-->\r\n<header class=\"oc-hero-section\">\r\n  <div class=\"graph-bg\">\r\n    <div class=\"container pt-80\">\r\n      <div class=\"row text-center align-items-center\" id=\"top\">\r\n        <div class=\"col-12\">\r\n          <div class=\"head\">\r\n            <h1 class=\"testuse__title\">สุขภาพดีคือของขวัญที่มีค่าที่สุด</h1>\r\n            <h2 class=\"testuse__detail\">ค้นหาแพ็กเกจตรวจสุขภาพที่หมาะสม <br class=\"hidden-lg-up\">พร้อมรายงานผลสุขภาพออนไลน์</h2>\r\n            <!-- <button type=\"button\" (click)=\"analyse()\" class=\"btn btn-primary btn-default cta-btn\" name=\"button\">\r\n                   วิเคราะห์สุขภาพ →\r\n                 </button> -->\r\n          </div>\r\n          <div class=\"filter-container\">\r\n            <div class=\"row\">\r\n              <div class=\"col-xl-2 col-lg-6\">\r\n                <label class=\"chevron\">\r\n                    <select class=\" form-location\" id=\"location\" name=\"location\" [(ngModel)]=\"filterLocation\">\r\n                        <option class=\"text-center\">เชียงใหม่</option>\r\n                    </select>\r\n                </label>\r\n              </div>\r\n              <div class=\"col-xl-2 col-lg-6\">\r\n                <label class=\"chevron\">\r\n                    <select class=\" form-gender\" id=\"gender\" name=\"gender\" [(ngModel)]=\"filterGender\">\r\n                        <option value=\"null\" disabled selected>เพศ</option>\r\n                        <option class=\"text-center\" [value]=\"1\">เพศชาย</option>\r\n                        <option class=\"text-center\" [value]=\"2\">เพศหญิง</option>\r\n                    </select>\r\n                </label>\r\n              </div>\r\n              <div class=\"col-xl-2 col-lg-6\">\r\n                <label class=\"chevron\">\r\n                    <select class=\" form-age\" id=\"age\" [(ngModel)]=\"filterAge\" name=\"age\">\r\n                        <option value=\"null\" disabled selected>อายุ</option>\r\n                        <option class=\"text-center\" *ngFor=\"let age of ageOptions   \" [value]=\"age\">{{age}}</option>\r\n                    </select>\r\n                </label>\r\n              </div>\r\n              <div class=\"col-xl-3 col-lg-6 text-left\">\r\n                <app-risk-selector (onChange)=\"onChangeRisk($event)\"></app-risk-selector>\r\n              </div>\r\n              <div class=\"col-xl-3 col-lg-12 mt-lg-0 mt-md-2 text-right\">\r\n                <button class=\"default-button\" (click)=\"matchPackages()\"><img class=\"search\" alt=\"ic_search.svg\" src=\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMThweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMTggMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz48L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9ImljX3NlYXJjaCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuMDAwMDAwLCAtMy4wMDAwMDApIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZT0iI0ZGRkZGRiI+CiAgICAgICAgICAgIDxnIGlkPSJzZWFyY2giPgogICAgICAgICAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNC4wMDAwMDAsIDQuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICAgICAgPGNpcmNsZSBpZD0iT3ZhbC04IiBjeD0iNyIgY3k9IjciIHI9IjciPjwvY2lyY2xlPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNiwxNiBMMTIuNDY0NDY2MSwxMi40NjQ0NjYxIiBpZD0iTGluZSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj48L3BhdGg+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==\"/>ค้นหาแพ็กเกจ</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</header>\r\n<!--End Header-->\r\n\r\n<!-- Start screw -->\r\n<div class=\"screw\"></div>\r\n<!-- End screw -->\r\n\r\n<!-- Start section1 -->\r\n<section class=\"section1\">\r\n   <div class=\"container my-lg-3 py-lg-5\">\r\n    <div class=\"row align-items-center\">\r\n      <div class=\"col-lg-7 push-lg-5 text-right\">\r\n        <img class=\"img-fluid\" src=\"assets/img/gage-and-mini.png\">\r\n      </div>\r\n      <div class=\"col-lg-5 pull-lg-7 text-lg-right text-md-center tex-center\">\r\n        <img class=\"img-fluid occlogo\" src=\"assets/img/occlogo.png\"> <br>\r\n        <h3>วิเคราะห์สุขภาพ</h3>\r\n        <p>กำหนดเป้าหมายสุขภาพ<br>ทราบภาวะที่ควรได้รับการตรวจค้นหา </p>\r\n\r\n        <button type=\"button\" class=\"btn btn-primary btn-blue\" (click)=\"analyse()\">เริ่มวิเคราะห์สุขภาพ</button><br><br>\r\n        <span>อ้างอิงข้อมูล จาก Medical Evidence </span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- End section1 -->\r\n\r\n<!-- Start section2 -->\r\n<section class=\"section2\">\r\n  <div class=\"container pt-80 pb-80\">\r\n    <div class=\"col-12\">\r\n      <h1>แพ็กเกจไหนเหมาะกับคุณ</h1>\r\n    </div>\r\n    <div class=\"row align-items-center slide-show\">\r\n\r\n      <div class=\"col-lg-4\">\r\n        <div class=\"package bgpkg1\">\r\n          <div class=\"pkg-desc\">\r\n            <h5>ตรวจก่อนแต่ง ( Exclusive Package)</h5>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">15,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-4\">\r\n        <div class=\"package bgpkg2\">\r\n          <div class=\"pkg-desc\">\r\n            <h5>หญิงไทยไกลมะเร็ง</h5>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">1,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-4\">\r\n        <div class=\"package bgpkg3\">\r\n          <div class=\"pkg-desc\">\r\n            <h5>โรคติดต่อทางเพศสัมพันธ์</h5>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">1,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div id=\"carouselExampleIndicators\" class=\"carousel slide slide-show-none\" data-ride=\"carousel\">\r\n      <ol class=\"carousel-indicators\">\r\n        <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>\r\n        <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\"></li>\r\n        <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\"></li>\r\n      </ol>\r\n      <div class=\"carousel-inner\">\r\n        <div class=\"carousel-item active\">\r\n          <img class=\"d-block w-100\" src=\"assets/img/p1.png\" alt=\"First slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3>ตรวจก่อนแต่ง ( Exclusive Package)</h3>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">15,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n        <div class=\"carousel-item\">\r\n          <img class=\"d-block w-100\" src=\"assets/img/p2.png\" alt=\"Second slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3>หญิงไทยไกลมะเร็ง</h3>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">1,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n        <div class=\"carousel-item\">\r\n          <img class=\"d-block w-100\" src=\"assets/img/p3.png\" alt=\"Third slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3>โรคติดต่อทางเพศสัมพันธ์</h3>\r\n            <p>\r\n              <span>ราคาเริ่มต้น</span>\r\n              <span class=\"price\">1,000 บาท</span>\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">\r\n        <!--<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>-->\r\n        <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\r\n        <span class=\"sr-only\">Previous</span>\r\n      </a>\r\n      <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">\r\n        <!--<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>-->\r\n        <i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\r\n        <span class=\"sr-only\">Next</span>\r\n      </a>\r\n    </div>\r\n\r\n\r\n  </div>\r\n</section>\r\n<!-- End section2 -->\r\n\r\n<!-- Start section3 -->\r\n<section class=\"section3\">\r\n  <div class=\"container pt-80 pb-80\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12\">\r\n        <h1>People Saying</h1>\r\n      </div>\r\n      <div class=\"col-xl-4\">\r\n        <div class=\"pplcard\">\r\n          <img src=\"assets/img/people1.png\">\r\n          <h6>\r\n            Aomamp Emily <br>\r\n            <small>อายุ 28 ปี , เจ้าของกิจการ</small>\r\n          </h6>\r\n          <p>\r\n            ขอบคุณ <span class=\"highlight\">#OptimizeCare</span> ที่ ช่วยให้ออม <span class=\"highlight\">#บันทึกผลตรวจสุขภาพ #ออนไลน์</span>            ส่วนตัวของออมที่สำคัญ ตรวจครั้งหน้าหรือปีไหนก็เทียบผลการตรวจได้อย่างสะดวกค่ะ\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 mt-xl-0 mt-5\">\r\n        <div class=\"pplcard\">\r\n          <img src=\"assets/img/people2.png\">\r\n          <h6>\r\n            Patty Wann <br>\r\n            <small>อายุ 33 ปี , เจ้าของกิจการ</small>\r\n          </h6>\r\n          <p>\r\n            อยากหาแพ็กเกจ <span class=\"highlight\">#ตรวจสุขภาพให้คุณแม่</span> แต่ไม่รู้จะเริ่มยังไง คุณแม่ควรตรวจอะไรบ้าง\r\n            กังวลอยาก <span class=\"highlight\">#คัดกรองมะเร็ง</span> ให้คุณแม่ <span class=\"highlight\">#OCareChecker</span>            ช่วยให้ข้อมูล สะดวกมากๆ\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-4 mt-xl-0 mt-5\">\r\n        <div class=\"pplcard\">\r\n          <img src=\"assets/img/people3.png\">\r\n          <h6>\r\n            Gopp Patiphan <br>\r\n            <small>อายุ 30 ปี , พนักงานประจำ</small>\r\n          </h6>\r\n          <p>\r\n            <span class=\"highlight\">#ออฟติไมซ์แคร์</span> ช่วยผมค้นหา <span class=\"highlight\">#แพ็กเกจ</span>เช็คอัพจากรพ.ใกล้บ้าน\r\n            และ <span class=\"highlight\">#เช็คราคา</span> ก่อนซื้อได้ครับ สบายใจ และสะดวกที่บันทึกผลตรวจเก็บไว้ได้ครับ\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- End section3 -->\r\n\r\n<!-- Start section4 -->\r\n<section class=\"section4\">\r\n  <div class=\"container pt-80 pb-80\">\r\n    <div class=\"row align-items-center\">\r\n      <div class=\"col-lg-6 text-lg-left text-center\">\r\n        <h1>ผลตรวจสุขภาพออนไลน์</h1>\r\n        <p>สะดวกโดยรายงานผลข้อมูลสุขภาพ<br>ส่งตรงจากโรงพยาบาลชั้นนำ<br>ทุกที่ ทุกเวลา</p>\r\n      </div>\r\n      <div class=\"col-lg-6 text-center\">\r\n        <img class=\"img-fluid\" src=\"assets/img/secure.png\">\r\n      </div>\r\n      <div class=\"col-lg-6 text-lg-left text-center\">\r\n        <h1>มาตรฐาน ปลอดภัย</h1>\r\n        <p>เฉพาะคุณเท่านั้นที่สามารถ<br>เข้าดูข้อมูลสุขภาพได้</p>\r\n      </div>\r\n      <div class=\"col-lg-6 text-lg-left text-center\">\r\n        <h1>เข้ารหัสข้อมูล</h1>\r\n        <p>Optimize Care ให้บริการ<br>บนความปลอดภัย <b>SSL Security</b></p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"row align-items-center\">\r\n      <div class=\"col-lg-6 push-lg-6 text-lg-left text-center pb-80\">\r\n        <div class=\"row\">\r\n          <div class=\"col-12\">\r\n            <h1>ค้นหาแพ็กเกจตรวจสุขภาพ</h1>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-12 feature-item\">\r\n            <br>\r\n            <p>ส่วนลดจากโรงพยาบาลชั้นนำ<br>ที่ได้รับมาตรฐาน ISO&JCI</p>\r\n            <button type=\"button\" class=\"btn btn-primary btn-blue\" (click)=\"goToTop()\">\r\n            \t\t\t<img class=\"search\" alt=\"ic_search.svg\" src=\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMThweCIgaGVpZ2h0PSIxOHB4IiB2aWV3Qm94PSIwIDAgMTggMTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8ZGVmcz48L2RlZnM+CiAgICA8ZyBpZD0iU3ltYm9scyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9ImljX3NlYXJjaCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMuMDAwMDAwLCAtMy4wMDAwMDApIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZT0iI0ZGRkZGRiI+CiAgICAgICAgICAgIDxnIGlkPSJzZWFyY2giPgogICAgICAgICAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNC4wMDAwMDAsIDQuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICAgICAgPGNpcmNsZSBpZD0iT3ZhbC04IiBjeD0iNyIgY3k9IjciIHI9IjciPjwvY2lyY2xlPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNiwxNiBMMTIuNDY0NDY2MSwxMi40NjQ0NjYxIiBpZD0iTGluZSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIj48L3BhdGg+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==\"/>ค้นหาแพ็กเกจ</button>\r\n            <br><br>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-6 pull-lg-6 text-center\">\r\n        <img class=\"img-fluid\" src=\"assets/img/mac.png\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- End section4 -->\r\n\r\n<!-- Start footer -->\r\n<footer class=\"pt-40\">\r\n  <div class=\"graph-bg\">\r\n    <div class=\"container\">\r\n      <div class=\"row text-lg-left text-center pt-40\">\r\n        <div class=\"col-lg-4 col-md-12\">\r\n          <div class=\"logo\">\r\n            <img src=\"assets/img/logo_op.png\" />\r\n          </div>\r\n        </div>\r\n        <div class=\"col-lg-4 col-md-6\">\r\n          <div>\r\n            <!-- <h3> Follow us  </h3> -->\r\n            <!-- <br> -->\r\n            <a href=\"#\">\r\n              <p>เกี่ยวกับเรา</p>\r\n            </a>\r\n            <a href=\"#\">\r\n              <p>นโยบายความเป็นส่วนตัว</p>\r\n            </a>\r\n            <a href=\"#\">\r\n              <p>ข้อกำหนด และ เงื่อนไขการให้บริการ</p>\r\n            </a>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-lg-4 col-md-6\">\r\n          <div class=\"contact\">\r\n            <h5> Contact us </h5>\r\n            <a class=\"call-me\" href=\"tel:0991319919\">099-131-9919</a>\r\n            <p >Optimizecare</p>\r\n            <p>MDNA Workspace</p>\r\n            <p> Chiangmai, Thailand </p>\r\n            <p>jo@optimize.care</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</footer>\r\n<!-- End footer  -->\r\n"

/***/ }),

/***/ "../../../../../src/app/lander-v2/lander-v2.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanderV2Component; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LanderV2Component = (function () {
    function LanderV2Component() {
        this.ageOptions = Array.apply(null, Array(60)).map(function (x, i) { return i + 15; });
        this.filterLocation = "เชียงใหม่";
        this.filterGender = null;
        this.filterAge = null;
        this.filterRisk = [];
    }
    LanderV2Component.prototype.ngOnInit = function () {
    };
    LanderV2Component.prototype.onChangeRisk = function (risks) {
        //when change list emited
        this.filterRisk = risks;
        console.log("current risks: " + risks);
    };
    LanderV2Component.prototype.printFilterData = function () {
        console.log("================ filter data");
        console.log("location: " + this.filterLocation);
        console.log("gender: " + this.filterGender);
        console.log("age: " + this.filterAge);
        console.log("risk: " + this.filterRisk);
        console.log("================");
    };
    LanderV2Component.prototype.analyse = function () {
        //this.router.navigate(['/checker']);
    };
    LanderV2Component.prototype.signin = function () {
        //this.router.navigate(['/authen']);
    };
    LanderV2Component.prototype.signup = function () {
        //this.router.navigate(['/authen/signup']);
    };
    LanderV2Component.prototype.goToTop = function () {
        //this._window.scrollTo(0, 0);
    };
    LanderV2Component.prototype.matchPackages = function () {
        if (this.filterAge && this.filterGender) {
            var filter = { location: this.filterLocation, gender: this.filterGender, age: this.filterAge, risks: this.filterRisk };
            //this.router.navigate(['/dashboard/packages/search']);
        }
        else {
            alert("กรุณากรอกข้อมูลให้ครบถ้วนก่อนค้นหาแพ็กเกจ");
        }
    };
    return LanderV2Component;
}());
LanderV2Component = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-lander-v2',
        template: __webpack_require__("../../../../../src/app/lander-v2/lander-v2.component.html"),
        styles: [__webpack_require__("../../../../../src/app/lander-v2/lander-v2.component.css")]
    }),
    __metadata("design:paramtypes", [])
], LanderV2Component);

//# sourceMappingURL=lander-v2.component.js.map

/***/ }),

/***/ "../../../../../src/app/lander-v2/risk-selector/risk-selector.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-control form-risk\" (click)=\"onSelect()\" *ngIf=\"totalSelected == 0\">\r\n    ความเสี่ยงสุขภาพ\r\n</div>\r\n<div class=\"form-control form-risk\" (click)=\"onSelect()\" *ngIf=\"totalSelected != 0\">\r\n    {{totalSelected}} ความเสี่ยง\r\n</div>\r\n<div class=\"risk-selector-container\" *ngIf=\"isOpen\">\r\n    <div class=\"row\">\r\n        <div class=\"tabSec\">\r\n            <h6 class=\"risk-title\">ความเสี่ยงสุขภาพ</h6>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"risks.smoking\" (change)=\"change()\"> การสูบบุหรี่\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"risks.drinking\" (change)=\"change()\"> ดื่มสุรา\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"risks.safesex\" (change)=\"change()\"> เคยมีโรคติดต่อทางเพศสัมพันธ์\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <div class=\"tabSec\">\r\n            <h6 class=\"risk-title\">ประวัติในครอบครัว</h6>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"fams.diabetes\" (change)=\"change()\"> โรคเบาหวาน\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"fams.heart\" (change)=\"change()\"> โรคหัวใจ\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"fams.bp\" (change)=\"change()\"> ความดันโลหิตสูง\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"fams.fat\" (change)=\"change()\"> ไขมันผิดปกติ\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"fams.bone\" (change)=\"change()\"> กระดูกพรุน หรือ กระดูกหักในครอบครัว\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <div class=\"tabSec\">\r\n            <h6 class=\"risk-title\">มะเร็งในครอบครัว</h6>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"cancer.breast\" (change)=\"change()\"> มะเร็งเต้านม\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"cancer.ovary\" (change)=\"change()\"> มะเร็งรังไข่\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"cancer.intestine\" (change)=\"change()\"> มะเร็งลำไส้\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <!-- <div class=\"col-3\">\r\n            <h6 class=\"risk-title\">เพิ่มเติม</h6>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.cancer\" (change)=\"change()\"> คัดกรองมะเร็ง\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.vaccine\" (change)=\"change()\"> ฉีดวัคซีน\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.allergy\" (change)=\"change()\"> ตรวจภูมิแพ้\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.marriage\" (change)=\"change()\"> ตรวจสุขภาพก่อนแต่งงาน\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.teeth\" (change)=\"change()\"> ตรวจฟันประจำปี\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.elderly\" (change)=\"change()\"> ตรวจสุขภาพชะลอวัย\r\n                </label>\r\n            </div>\r\n            <div class=\"risk\">\r\n                <label class=\"form-check-label\">\r\n                    <input class=\"form-check-input\" type=\"checkbox\" [(ngModel)]=\"others.contact\" (change)=\"change()\"> ปรึกษาแพทย์\r\n                </label>\r\n            </div>\r\n        </div> -->\r\n    </div>\r\n    <div class=\"link-container\">\r\n        <a class=\"text-style1\" (click)=\"clear()\">ล้าง</a>\r\n        <a class=\"text-style2\" (click)=\"onSelect()\">นำไปใช้</a>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/lander-v2/risk-selector/risk-selector.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Kanit);", ""]);

// module
exports.push([module.i, "@charset \"UTF-8\";\nbody, p, a, button, span, input, ol, ul, label {\n  font-family: 'Sukhumvit Set', 'Kanit',  sans-serif; }\n\nh1, h2, h3, h4, h5, h6, u {\n  font-family: 'Sukhumvit Set', 'Kanit',  sans-serif;\n  font-weight: 600; }\n\nh1 {\n  font-size: 50px; }\n\nh2 {\n  font-size: 45px;\n  font-weight: 300; }\n\nh3 {\n  font-size: 25px;\n  font-weight: 400; }\n\nh4 {\n  font-size: 20px; }\n\nh5 {\n  font-size: 20px;\n  font-weight: 400; }\n\nh6 {\n  font-size: 19px;\n  font-weight: 500; }\n\np {\n  font-size: 20px;\n  font-weight: 300; }\n\n.risk-selector-container {\n  position: absolute;\n  width: 700px;\n  background: #FFFFFF;\n  border: 1px solid #CFCFCF;\n  padding: 20px;\n  right: 0;\n  z-index: 9; }\n\n.risk {\n  padding-top: 6px; }\n\n.risk-title {\n  font-family: 300;\n  font-size: 14px;\n  color: #7D7C7D;\n  letter-spacing: 0; }\n\n.link-container {\n  padding: 20px 0; }\n\n.text-style1 {\n  float: left;\n  color: #4DCCF4 !important; }\n\n.text-style2 {\n  float: right;\n  font-weight: 600;\n  font-size: 18px;\n  color: #F5A623 !important;\n  letter-spacing: 0; }\n\n.form-risk {\n  position: relative;\n  color: #7D7C7D;\n  padding-left: 40px;\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAVCAYAAABCIB6VAAAAAXNSR0IArs4c6QAAAxdJREFUOBGVVEtoU1EQnbnvtbG1iy4U3VhrPogUXDS6saKIutCqC4uNSVBQXFgXheInTRfSXdPoxk+RgoiCTdOoFCm4ENT6WbgRFFFRmyqUWlTEb21Cc+84t+mFNOknDoRzZ+bMybyZ+x5Ckebu7D2giJpFeeXGoead6YXKxEIEkyeCBiBaZ038qTax+bBoYWDV+YTyc0UJOyOJKi5cooulmlxbk0iU5gvl+5gfyPXdkViNAriqR5Ab59bHBYpIrdPquNHYKHNz5jynsDMabwIlzwFBCSC8JRCPEGEUlNrAwnVcuBgQnzscjp1vWhrGjKBB2xxy0RPp2yRlpotFx9EWB5Kn/H06X9OVqJj4TfeEtegdyNQVIqpPp1IJxk2IOGMHBTP2dg+US8okuBuwhF1vRLVw6vfkBQD7Z/Lk3i9DIf9uAHzM4Y2eM/FjOp9rBcI/vv/aw4RlXHTtfavvkSG7or0+AvR5nfBax3SHWIpBPpJSdMLwDE7N2NMZ286XicWYBRTg8w4hxD7u6qaOrTl7a2Uqk3rND1vCkk8dixw+M1dnJPaBl1vN/COMaQWoyoR133ZGezxS0l0tMMOI3hg/nUkfZdFv7K/gRd4xotP5V4zVSqnLWZ8gJeGGSJ4MDAGKoEA8rn8I+DBLEO7pQhhuDYT50fr58Z8cDPmjJj6FRKs1CoFtuh4EtoDDarOntxkzZFdn7Bso2Ewkt3HstolzpyuZ7GtH5KudNff5xFL1N7OKRzA2FAp0mLjGguVhWWU/D/oHITS5o/FaTWonEhbBxbch/yftG1MT8gqLWrzJ8yZmcGp5xjHoicZ3SSkHeCxfObY/GQ7cNzmN2fusXx51mEVfeJ22N/8NnFVYF7s7etsVqNNcqK/KYwH0QCF85luxniO7+aXgbwd+tIG2vgsHh3VNrs0prEmuaF8dSXmdlatnFmGGELurli0/MXhoSyo3Z87zChuSq6PHyzO/xJ2v52UfLSdKvAwHv5v8bFiwvNlIyXDwGc97VOdstAcXEtW8ooQ18X+taGH+bA5y1yMVlWUjxfzJP8JVJzbUez84AAAAAElFTkSuQmCC);\n  background-repeat: no-repeat;\n  background-position: 10px center; }\n\n.form-risk::after {\n  content: '\\25BE';\n  position: absolute;\n  width: 24px;\n  color: #2287A5;\n  font-weight: bold;\n  font-size: 18px;\n  right: 2px;\n  bottom: 6px;\n  pointer-events: none;\n  z-index: 2;\n  font-family: initial; }\n\n@media screen and (min-width: 320px) and (max-width: 750px) {\n  .risk-selector-container {\n    width: 100%; }\n  .risk-selector-container .row .tabSec {\n    margin: 5px 0; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/lander-v2/risk-selector/risk-selector.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiskSelectorComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { User } from '../../../../data/user';
// import { UserService } from '../../../../data/user.service';
var RiskSelectorComponent = (function () {
    function RiskSelectorComponent() {
        // set default value from user service data
        // this.user = this.userService.getUserData();
        //
        // if(this.user.getFilterRisks().alcohol) {this.risks.drinking = true;}
        // if(this.user.getFilterRisks().smoke) {this.risks.smoking = true;}
        // if(this.user.getFilterRisks().sex) {this.risks.safesex = true;}
        //
        // if(this.user.heredity.includes("FM_DM")) {this.fams.diabetes = true;}
        // if(this.user.heredity.includes("FM_CARDIO")) {this.fams.heart = true;}
        // if(this.user.heredity.includes("FM_HTN")) {this.fams.bp = true;}
        // if(this.user.heredity.includes("FM_DLP")) {this.fams.fat = true;}
        // if(this.user.heredity.includes("FM_OSTEOPOROSIS")) {this.fams.bone = true;}
        //
        // if(this.user.heredity.includes("FM_CABREAST")) {this.cancer.breast = true;}
        // if(this.user.heredity.includes("FM_CAOVARY")) {this.cancer.ovary = true;}
        // if(this.user.heredity.includes("FM_CACOLON")) {this.cancer.intestine = true;}
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* EventEmitter */]();
        this.risks = { smoking: false, drinking: false, safesex: false };
        this.fams = { diabetes: false, heart: false, bp: false, fat: false, bone: false };
        this.cancer = { breast: false, ovary: false, intestine: false };
        this.others = { cancer: false, vaccine: false, allergy: false, marriage: false, teeth: false, elderly: false, contact: false };
        // user: User;
        this.riskList = [];
        this.isOpen = false;
        this.totalSelected = this.riskList.length;
    }
    RiskSelectorComponent.prototype.ngOnInit = function () {
        // reset value with input directive
        if (this.reset = true) {
            this.clear();
            this.totalSelected = this.riskList.length;
        }
        // console.log("risk list issssss: " + this.riskList);
        if (this.inputRisks) {
            if (this.inputRisks.includes("ALCOHOL")) {
                this.risks.drinking = true;
            }
            if (this.inputRisks.includes("SMOKING")) {
                this.risks.smoking = true;
            }
            if (this.inputRisks.includes("SEXRISK")) {
                this.risks.safesex = true;
            }
            if (this.inputRisks.includes("FM_DM")) {
                this.fams.diabetes = true;
            }
            if (this.inputRisks.includes("FM_CARDIO")) {
                this.fams.heart = true;
            }
            if (this.inputRisks.includes("FM_HTN")) {
                this.fams.bp = true;
            }
            if (this.inputRisks.includes("FM_DLP")) {
                this.fams.fat = true;
            }
            if (this.inputRisks.includes("FM_OSTEOPOROSIS")) {
                this.fams.bone = true;
            }
            if (this.inputRisks.includes("FM_CABREAST")) {
                this.cancer.breast = true;
            }
            if (this.inputRisks.includes("FM_CAOVARY")) {
                this.cancer.ovary = true;
            }
            if (this.inputRisks.includes("FM_CACOLON")) {
                this.cancer.intestine = true;
            }
            if (this.inputRisks.includes("CANCER")) {
                this.others.cancer = true;
            }
            if (this.inputRisks.includes("MISC_VACCINE")) {
                this.others.vaccine = true;
            }
            if (this.inputRisks.includes("MISC_ALLERGY")) {
                this.others.allergy = true;
            }
            if (this.inputRisks.includes("MISC_MARRIAGE")) {
                this.others.marriage = true;
            }
            if (this.inputRisks.includes("DENTAL")) {
                this.others.teeth = true;
            }
            if (this.inputRisks.includes("MISC_ANTIAGING")) {
                this.others.elderly = true;
            }
            if (this.inputRisks.includes("MISC_DOCTORMEET")) {
                this.others.contact = true;
            }
            this.change();
        }
    };
    RiskSelectorComponent.prototype.onSelect = function () {
        this.isOpen = !this.isOpen;
        this.change();
    };
    RiskSelectorComponent.prototype.change = function () {
        // console.log("===========");
        // console.log(this.risks);
        // console.log(this.fams);
        // console.log(this.cancer);
        // console.log(this.others);
        this.riskList = [];
        if (this.risks.smoking) {
            this.riskList.push("SMOKING");
        }
        if (this.risks.drinking) {
            this.riskList.push("ALCOHOL");
        }
        if (this.risks.safesex) {
            this.riskList.push("SEXRISK");
        }
        if (this.fams.diabetes) {
            this.riskList.push("FM_DM");
        }
        if (this.fams.heart) {
            this.riskList.push("FM_CARDIO");
        }
        if (this.fams.bp) {
            this.riskList.push("FM_HTN");
        }
        if (this.fams.fat) {
            this.riskList.push("FM_DLP");
        }
        if (this.fams.bone) {
            this.riskList.push("FM_OSTEOPOROSIS");
        }
        if (this.cancer.breast) {
            this.riskList.push("FM_CABREAST");
        }
        if (this.cancer.ovary) {
            this.riskList.push("FM_CAOVARY");
        }
        if (this.cancer.intestine) {
            this.riskList.push("FM_CACOLON");
        }
        if (this.others.cancer) {
            this.riskList.push("CANCER");
        }
        if (this.others.vaccine) {
            this.riskList.push("MISC_VACCINE");
        }
        if (this.others.allergy) {
            this.riskList.push("MISC_ALLERGY");
        }
        if (this.others.marriage) {
            this.riskList.push("MISC_MARRIAGE");
        }
        if (this.others.teeth) {
            this.riskList.push("DENTAL");
        }
        if (this.others.elderly) {
            this.riskList.push("MISC_ANTIAGING");
        }
        if (this.others.contact) {
            this.riskList.push("MISC_DOCTORMEET");
        }
        this.totalSelected = this.riskList.length;
        this.onChange.emit(this.riskList);
    };
    RiskSelectorComponent.prototype.clear = function () {
        this.risks = { smoking: false, drinking: false, safesex: false };
        this.fams = { diabetes: false, heart: false, bp: false, fat: false, bone: false };
        this.cancer = { breast: false, ovary: false, intestine: false };
        this.others = { cancer: false, vaccine: false, allergy: false, marriage: false, teeth: false, elderly: false, contact: false };
    };
    return RiskSelectorComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* Output */])(),
    __metadata("design:type", Object)
], RiskSelectorComponent.prototype, "onChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Input */])(),
    __metadata("design:type", Boolean)
], RiskSelectorComponent.prototype, "reset", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Input */])(),
    __metadata("design:type", Object)
], RiskSelectorComponent.prototype, "inputRisks", void 0);
RiskSelectorComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* Component */])({
        selector: 'app-risk-selector',
        template: __webpack_require__("../../../../../src/app/lander-v2/risk-selector/risk-selector.component.html"),
        styles: [__webpack_require__("../../../../../src/app/lander-v2/risk-selector/risk-selector.component.scss")]
    }),
    __metadata("design:paramtypes", [])
], RiskSelectorComponent);

//# sourceMappingURL=risk-selector.component.js.map

/***/ }),

/***/ "../../../../../src/assets/img/graph-bg-mobile.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "graph-bg-mobile.d74beabe4ca05606be16.png";

/***/ }),

/***/ "../../../../../src/assets/img/graph-bg.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "graph-bg.249b7a0ebe82e525549a.png";

/***/ }),

/***/ "../../../../../src/assets/img/ocbg.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ocbg.1be6b9928c983934a572.png";

/***/ }),

/***/ "../../../../../src/assets/img/p1.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "p1.c6adf2951595686b35d2.png";

/***/ }),

/***/ "../../../../../src/assets/img/p2.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "p2.c88d58955cae0d4054fe.png";

/***/ }),

/***/ "../../../../../src/assets/img/p3.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "p3.5341645b5142a6f208e7.png";

/***/ }),

/***/ "../../../../../src/assets/img/phone.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAfCAYAAAAWRbZDAAAAAXNSR0IArs4c6QAAAiJJREFUSA2tlj9LXEEUxfetxjWgjaBYGBFBGwsbwaDYKtgJFokQUYR0NoKlWGgl2gUiiH4ASwtt0gQEQVQEv4BIUNkijaIbw+76G93Bu8ObPzI7cHbm3nvmnDfvzb55mYyllcvlTjAB6i2U2qQxmAWPpVKJrrxcG1VDBeEc2FImGsQ3oM6gxoUIdoFTbSJ78uNx6mI2YgPgrzSQY2q7gh43ROxciptj6gXQEueSyWQRaQL9LiHqOepfXJyQWhZSUwgRzkwgz0pLuOpGqg/0iZVFIUmSe9Ds4vhqWQQKkPI+IvWdAI6fwqoOzE2hY2pFsORbud+lwkBoXoubPbWfwUIeotogqu2/dqm/PanZmCQrODFXpWLaE+iI0dZz9cpU/EMnZY/RB+IFmYseI9oALi2rU2+Q3mgTKYDgXJpZ5XYeUa/d2YaYen2lPruK4bq8uOgxZuoEKDpW+DXaRApgtuIw+0d9VPKjxojVg0OP4bQ0ga8+JfLgCkzKmnfMhE/g1mao8rQd0Ac2TR65DRC+oSAPgpcPH1MsJGbub9DuXZkmQJ4CaL99BL1nzNxrMKL1vD3k75GG/5n/zXlgyquAPEO8TS9fcZLiHHNuXjgJZhGjMWD9CrPdXuaolVXtXlM7NWZSNzi2CZt5uH/AcKpYSJLJdWAR3JniMqb+C7SFaHo5CLWCNZA3TNSfcBVUPd/gDeJyRlSdeUPgM/gI9tgQZ/RV7Rns3tlhU1tYvwAAAABJRU5ErkJggg=="

/***/ }),

/***/ "../../../../../src/assets/lander/facebook_nocircle.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAAAAXNSR0IArs4c6QAAAUJJREFUaAXtljEKAkEMRV21thG0sBHEM4gXsPMI3sLKI3ghC/ECNiJiJ1iICoKVWsr6V6YYZAYGTCTCXwg7mx2S/Jc4bqnEiwRIgARIgARIgARIgASMEMh+VUee53XkGsK6sJqX95Bl2dR7tr+EmBHsBgtdK/sKvAqhoAd7hpQ4n6igspdbazlB4F/keddf1VJRxEUHit/oIJDjDN/W+XeB9zZdENSMjFpHq2LtUYhNwEVLUCzhV/nQlRYCtGGNSKA+9jzcuzuO7XVknw03ih1HRi3knktWrT1yKbWeUjal7rEg6JhabMo+C4JEDwiVQwEk97CZI1r8D1XcurhdYUvveeOt7S9xCnx+wy00q7YwcqL6KEgUp0IwdkgBqmhIdkgUp0IwdkgBqmhIdkgUJ4ORAAmQAAmQAAmQAAmQAAn8EYEXeom7dsSnFssAAAAASUVORK5CYII="

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-Black.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-Black.b1fe792e1c8abd677226.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-Bold.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-Bold.b6c703554d0fcb4fe829.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-BoldItalic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-BoldItalic.5336a003dc1275799293.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-ExtraBold.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-ExtraBold.e7ea8256f57125280ae3.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-ExtraLight.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-ExtraLight.c6ad0b6bac782e356c47.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-ExtraLightItalic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-ExtraLightItalic.ede1327fe385b38b2500.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-Italic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-Italic.3e28c021b4bc48a20495.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-Light.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-Light.db7573abdbbc69ca0828.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-LightItalic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-LightItalic.dd736766dbcba398009b.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-Regular.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-Regular.092b215e7fd545172900.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-SemiBold.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-SemiBold.b0a3f63106ef1f500a36.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-SemiboldItalic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-SemiboldItalic.1ec45d598ce8dbbef435.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-UltraBold.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-UltraBold.d995eee0b837342f0057.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-UltraLight.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-UltraLight.56afd81dedd7007d0d2c.ttf";

/***/ }),

/***/ "../../../../../src/assets/media/font/ThaiSansNeue-UltraLightItalic.ttf":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ThaiSansNeue-UltraLightItalic.e9117a5c911291eb4fb9.ttf";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map