import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';

// import { User } from '../../../../data/user';
// import { UserService } from '../../../../data/user.service';

@Component({
  selector: 'app-risk-selector',
  templateUrl: './risk-selector.component.html',
  styleUrls: ['./risk-selector.component.scss']
})
export class RiskSelectorComponent implements OnInit {

	@Output() onChange = new EventEmitter<any>();
  @Input() reset?: boolean;
  @Input() inputRisks?;

	risks = {smoking: false, drinking: false, safesex: false};
	fams = {diabetes: false, heart: false, bp: false, fat:false, bone: false};
	cancer = {breast: false, ovary: false, intestine: false};
	others = {cancer: false, vaccine: false, allergy: false, marriage: false, teeth: false, elderly: false, contact: false};

 // user: User;
	riskList = [];
	isOpen:boolean = false;
	totalSelected:number;

  	constructor() {
      // set default value from user service data
      // this.user = this.userService.getUserData();
      //
      // if(this.user.getFilterRisks().alcohol) {this.risks.drinking = true;}
      // if(this.user.getFilterRisks().smoke) {this.risks.smoking = true;}
      // if(this.user.getFilterRisks().sex) {this.risks.safesex = true;}
      //
      // if(this.user.heredity.includes("FM_DM")) {this.fams.diabetes = true;}
      // if(this.user.heredity.includes("FM_CARDIO")) {this.fams.heart = true;}
      // if(this.user.heredity.includes("FM_HTN")) {this.fams.bp = true;}
      // if(this.user.heredity.includes("FM_DLP")) {this.fams.fat = true;}
      // if(this.user.heredity.includes("FM_OSTEOPOROSIS")) {this.fams.bone = true;}
      //
      // if(this.user.heredity.includes("FM_CABREAST")) {this.cancer.breast = true;}
      // if(this.user.heredity.includes("FM_CAOVARY")) {this.cancer.ovary = true;}
      // if(this.user.heredity.includes("FM_CACOLON")) {this.cancer.intestine = true;}

      this.totalSelected = this.riskList.length;

    }

  	ngOnInit() {
      // reset value with input directive
      if(this.reset = true) {
        this.clear();
        this.totalSelected = this.riskList.length;
      }
      // console.log("risk list issssss: " + this.riskList);
      if(this.inputRisks) {
        if(this.inputRisks.includes("ALCOHOL")) {this.risks.drinking = true;}
        if(this.inputRisks.includes("SMOKING")) {this.risks.smoking = true;}
        if(this.inputRisks.includes("SEXRISK")) {this.risks.safesex = true;}

        if(this.inputRisks.includes("FM_DM")) {this.fams.diabetes = true;}
        if(this.inputRisks.includes("FM_CARDIO")) {this.fams.heart = true;}
        if(this.inputRisks.includes("FM_HTN")) {this.fams.bp = true;}
        if(this.inputRisks.includes("FM_DLP")) {this.fams.fat = true;}
        if(this.inputRisks.includes("FM_OSTEOPOROSIS")) {this.fams.bone = true;}

        if(this.inputRisks.includes("FM_CABREAST")) {this.cancer.breast = true;}
        if(this.inputRisks.includes("FM_CAOVARY")) {this.cancer.ovary = true;}
        if(this.inputRisks.includes("FM_CACOLON")) {this.cancer.intestine = true;}

        if(this.inputRisks.includes("CANCER")) {this.others.cancer = true;}
        if(this.inputRisks.includes("MISC_VACCINE")) {this.others.vaccine = true;}
        if(this.inputRisks.includes("MISC_ALLERGY")) {this.others.allergy = true;}
        if(this.inputRisks.includes("MISC_MARRIAGE")) {this.others.marriage = true;}
        if(this.inputRisks.includes("DENTAL")) {this.others.teeth = true;}
        if(this.inputRisks.includes("MISC_ANTIAGING")) {this.others.elderly = true;}
        if(this.inputRisks.includes("MISC_DOCTORMEET")) {this.others.contact = true;}

        this.change();
      }

  	}

  	onSelect() {
  		this.isOpen = !this.isOpen;
  		this.change();
  	}

  	change() {
  		// console.log("===========");
  		// console.log(this.risks);
  		// console.log(this.fams);
  		// console.log(this.cancer);
  		// console.log(this.others);

  		this.riskList = [];
  		if(this.risks.smoking) {this.riskList.push("SMOKING");}
  		if(this.risks.drinking) {this.riskList.push("ALCOHOL");}
  		if(this.risks.safesex) {this.riskList.push("SEXRISK");}

  		if(this.fams.diabetes) {this.riskList.push("FM_DM");}
  		if(this.fams.heart) {this.riskList.push("FM_CARDIO");}
  		if(this.fams.bp) {this.riskList.push("FM_HTN");}
  		if(this.fams.fat) {this.riskList.push("FM_DLP");}
  		if(this.fams.bone) {this.riskList.push("FM_OSTEOPOROSIS");}

  		if(this.cancer.breast) {this.riskList.push("FM_CABREAST");}
  		if(this.cancer.ovary) {this.riskList.push("FM_CAOVARY");}
  		if(this.cancer.intestine) {this.riskList.push("FM_CACOLON");}

  		if(this.others.cancer) {this.riskList.push("CANCER");}
  		if(this.others.vaccine) {this.riskList.push("MISC_VACCINE");}
  		if(this.others.allergy) {this.riskList.push("MISC_ALLERGY");}
  		if(this.others.marriage) {this.riskList.push("MISC_MARRIAGE");}
  		if(this.others.teeth) {this.riskList.push("DENTAL");}
  		if(this.others.elderly) {this.riskList.push("MISC_ANTIAGING");}
  		if(this.others.contact) {this.riskList.push("MISC_DOCTORMEET");}

  		this.totalSelected = this.riskList.length;

  		this.onChange.emit(this.riskList);
  	}

  	clear() {
  		this.risks = {smoking: false, drinking: false, safesex: false};
		  this.fams = {diabetes: false, heart: false, bp: false, fat:false, bone: false};
		  this.cancer = {breast: false, ovary: false, intestine: false};
		  this.others = {cancer: false, vaccine: false, allergy: false, marriage: false, teeth: false, elderly: false, contact: false};
  	}

}
