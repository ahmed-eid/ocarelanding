import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lander-v2',
  templateUrl: './lander-v2.component.html',
  styleUrls: ['./lander-v2.component.css']
})
export class LanderV2Component implements OnInit {

  ageOptions: number[] = Array.apply(null, Array(60)).map((x, i) => i + 15);

  filterLocation: string = "เชียงใหม่";
  filterGender: number = null;
  filterAge: number = null;
  filterRisk: any[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  onChangeRisk(risks: any[]) {
    //when change list emited
    this.filterRisk = risks;
    console.log("current risks: " + risks);
  }

  printFilterData() {
    console.log("================ filter data");
    console.log("location: " + this.filterLocation);
    console.log("gender: " + this.filterGender);
    console.log("age: " + this.filterAge);
    console.log("risk: " + this.filterRisk);
    console.log("================");
  }

  analyse() {
    //this.router.navigate(['/checker']);
  }

  signin() {
    //this.router.navigate(['/authen']);
  }

  signup() {
    //this.router.navigate(['/authen/signup']);
  }

  goToTop() {
    //this._window.scrollTo(0, 0);
  }

  matchPackages() {
    if (this.filterAge && this.filterGender) {
      let filter = { location: this.filterLocation, gender: this.filterGender, age: this.filterAge, risks: this.filterRisk };

      //this.router.navigate(['/dashboard/packages/search']);
    }
    else {
      alert("กรุณากรอกข้อมูลให้ครบถ้วนก่อนค้นหาแพ็กเกจ")
    }
  }

}
