import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LanderV2Component } from './lander-v2/lander-v2.component';
import { RiskSelectorComponent } from './lander-v2/risk-selector/risk-selector.component';
// import { NewFilterService } from './dashboard/packages/package-search/new-filter.service'
// import { WindowRefService } from './data/window-ref.service'

@NgModule({
  declarations: [
    AppComponent,
    LanderV2Component,
    RiskSelectorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
